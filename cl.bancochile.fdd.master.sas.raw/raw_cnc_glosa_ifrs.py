from pyspark.sql import SparkSession

spark = SparkSession.builder.appName("ingesta_glosa_ifrs").getOrCreate()

df = spark.read.format("csv").load("/GDD_M_CNC_GLOSA_IFRS.csv")

df.printSchema()
